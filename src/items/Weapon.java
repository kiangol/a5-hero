package items;

import hero.Slot;

public class Weapon extends Item {

    private final ItemType itemType = ItemType.WEAPON;
    private WeaponType weaponType;
    private double damage;
    private double attackSpeed;

    public Weapon(String name) {
        super(name);
    }

    public Weapon() {
        super();
    }

    public double getDPS() {
        return damage * attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
}

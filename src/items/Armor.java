package items;

import hero.Slot;

public class Armor extends Item {
    private ArmorType armorType;

    public Armor(String name, int requiredLevel, Slot slot) {
        super(name);
    }

    public Armor() {
        super();
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }
}
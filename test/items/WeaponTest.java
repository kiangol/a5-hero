package items;

import hero.InvalidWeaponException;
import hero.Slot;
import hero.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    @Test
    void equipWeapon_WarriorEquipHighLevelAxe_ShouldThrowInvalidWeaponException() {
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon();
        testAxe.setName("Axtreme Axe");
        testAxe.setLevel(2);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXES);
        testAxe.setDamage(12);
        testAxe.setAttackSpeed(1.2);
        assertThrows(InvalidWeaponException.class, () -> {
            assert warrior.equipWeapon(testAxe);
        });
    }

    @Test
    void equipWeapon_EquipWrongWeaponType_ShouldThrowInvalidWeaponException() {
        Warrior warrior = new Warrior();
        Weapon testBow = new Weapon();
        testBow.setName("Wrongbow");
        testBow.setLevel(2);
        testBow.setSlot(Slot.WEAPON);
        testBow.setWeaponType(WeaponType.BOWS);
        testBow.setDamage(12);
        testBow.setAttackSpeed(1.2);
        assertThrows(InvalidWeaponException.class, () -> {
            assert warrior.equipWeapon(testBow);
        });
    }

    @Test
    void equipWeapon_EquipValidWeapon_ShouldReturnTrue() {
        Warrior warrior = new Warrior();
        Weapon testAxe = new Weapon();
        testAxe.setName("Axtreme Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXES);
        testAxe.setDamage(12);
        testAxe.setAttackSpeed(1.2);
        assertTrue(warrior.equipWeapon(testAxe));
    }

    @Test
    void getDPS_CalculateDPSWithNoWeapon_ShouldReturnExpectedValue() {
        Warrior warrior = new Warrior();
        double expected = (1.0 + (5.0 / 100.0));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);

    }

    @Test
    void getDPS_CalculateDPSWithWeapon_ShouldReturnExpectedValue() {
        Warrior warrior = new Warrior();

        Weapon testAxe = new Weapon();
        testAxe.setName("Axtreme Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXES);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);
        warrior.equipWeapon(testAxe);

        double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);
    }
}
package hero;

import attributes.PrimaryAttribute;
import items.*;

public class Mage extends Hero {
    public Mage(String name) {
        super(name, new PrimaryAttribute(5, 1, 1, 8));
        setHeroType(HeroType.MAGE);
    }

    public Mage() {
        setHeroType(HeroType.MAGE);
    }

    @Override
    protected int getArmorAttribute(Armor armor) {
        return armor.getIntelligence();
    }

    @Override
    protected int getPrimaryAttribute() {
        return attributes.getIntelligence();
    }

    @Override
    public boolean equipWeapon(Weapon w) throws InvalidWeaponException {
        super.equipWeapon(w);
        if (w.getWeaponType() == WeaponType.STAFFS || w.getWeaponType() == WeaponType.WANDS) {
            equipment.put(w.getSlot(), w);
        } else {
            throw new InvalidWeaponException("This hero can only equip STAFFS, WANDS, weapon is " + w.getWeaponType());
        }
        return false;
    }

    @Override
    public boolean equipArmor(Armor a) throws InvalidArmorException {
        super.equipArmor(a);
        if (a.getArmorType() == ArmorType.CLOTH) {
            equipment.put(a.getSlot(), a);
            return true;
        } else {
            throw new InvalidArmorException("This hero can only equip CLOTH");
        }
    }

    @Override
    public void levelUp() {
        attributes.increaseVitality(3);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(1);
        attributes.increaseIntelligence(5);
        level++;
    }
}

package hero;

import attributes.PrimaryAttribute;
import items.*;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(name, new PrimaryAttribute(8,2,6,1));
        setHeroType(HeroType.ROGUE);
    }

    public Rogue() {
        setHeroType(HeroType.ROGUE);
    }

    @Override
    protected int getArmorAttribute(Armor armor) {
        return armor.getDexterity();
    }

    @Override
    protected int getPrimaryAttribute() {
        return attributes.getDexterity();
    }

    @Override
    public boolean equipWeapon(Weapon w) throws InvalidWeaponException {
        super.equipWeapon(w);
        if (w.getWeaponType() == WeaponType.DAGGERS || w.getWeaponType() == WeaponType.SWORDS) {
            equipment.put(w.getSlot(), w);
        } else {
            throw new InvalidWeaponException("This hero can only equip DAGGERS, SWORDS, weapon is " + w.getWeaponType());
        }
        return false;
    }

    @Override
    public boolean equipArmor(Armor a) throws InvalidArmorException {
        super.equipArmor(a);
        if (a.getArmorType() == ArmorType.LEATHER || a.getArmorType() == ArmorType.MAIL) {
            equipment.put(a.getSlot(), a);
            return true;
        } else {
            throw new InvalidArmorException("This hero can only equip LEATHER and MAIL armor");
        }
    }

    @Override
    public void levelUp() {
        System.out.println("ROGUE LEVEL UP");
        attributes.increaseVitality(3);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(4);
        attributes.increaseIntelligence(1);
        level++;
    }
}

package attributes;

public class PrimaryAttribute {

    int vitality, strength, dexterity, intelligence;

    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void increaseVitality(int vitality) {
        this.vitality += vitality;
    }

    public int getStrength() {
        return strength;
    }

    public void increaseStrength(int strength) {
        this.strength += strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void increaseDexterity(int dexterity) {
        this.dexterity += dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void increaseIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }
}

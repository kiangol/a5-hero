package hero;

public class InvalidArmorException extends RuntimeException {
    public InvalidArmorException(String msg) {
        super(msg);
    }
}

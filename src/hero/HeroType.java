package hero;

public enum HeroType {
    MAGE,
    RANGER,
    ROGUE,
    WARRIOR
}

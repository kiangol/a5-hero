import hero.*;


public class Main {
    public static void main(String[] args) {
        Hero h1 = new Mage("Mager");
        Hero h2 = new Ranger("RangerBoi");
        Hero h3 = new Rogue("RougerThat");
        Hero h4 = new Warrior("Warriorz");

        System.out.println(h1);
        System.out.println(h2);
        System.out.println(h3);
        System.out.println(h4);

        h4.levelUp();

        System.out.println(h4);
    }
}
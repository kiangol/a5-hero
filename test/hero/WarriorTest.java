package hero;

import attributes.PrimaryAttribute;
import items.Armor;
import items.ArmorType;
import items.Weapon;
import items.WeaponType;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class WarriorTest {
    @Test
    void getDPS_WithWeaponAndArmor() {
        Hero warrior = new Warrior();

        Weapon testAxe = new Weapon();
        testAxe.setName("Axtreme Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXES);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(2,1,0,0));

        warrior.equipArmor(testPlateBody);
        warrior.equipWeapon(testAxe);

        double expected = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));
        double actual = warrior.getDPS();
        assertEquals(expected, actual);
    }
}
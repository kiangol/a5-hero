# Assignment 1 - Hero thing
This is a command line "game" with different types of heroes, weapons, and armor. 
There are 4 types of heroes: mage, ranger, rogue, warrior. They are all subclasses of the Hero superclass. 
There are two types of items: Weapon and Armor.

# Testing
Tests are located in the `test` directory. They consist of unit tests checking the minimum functionality.
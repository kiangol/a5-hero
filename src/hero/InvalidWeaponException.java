package hero;

public class InvalidWeaponException extends RuntimeException {
    public InvalidWeaponException(String msg) {
        super(msg);
    }
}

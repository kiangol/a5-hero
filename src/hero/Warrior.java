package hero;

import attributes.PrimaryAttribute;
import items.*;

public class Warrior extends Hero {
    public Warrior(String name) {
        super(name, new PrimaryAttribute(10, 5, 2, 1));
        setHeroType(HeroType.WARRIOR);
    }

    public Warrior() {
        super(new PrimaryAttribute(10,5,2,1));
        setHeroType(HeroType.WARRIOR);
    }

    @Override
    protected int getArmorAttribute(Armor armor) {
        return armor.getStrength();
    }

    @Override
    protected int getPrimaryAttribute() {
        return attributes.getStrength();
    }

    @Override
    public boolean equipWeapon(Weapon w) throws InvalidWeaponException {
        super.equipWeapon(w);
        if (w.getWeaponType() == WeaponType.AXES || w.getWeaponType() == WeaponType.HAMMERS || w.getWeaponType() == WeaponType.SWORDS) {
            equipment.put(w.getSlot(), w);
            return true;
        } else {
            throw new InvalidWeaponException("This hero can only equip AXES, HAMMERS, SWORDS, weapon is " + w.getWeaponType());
        }
    }

    @Override
    public boolean equipArmor(Armor a) throws InvalidArmorException {
        super.equipArmor(a);
        if (a.getArmorType() == ArmorType.PLATE || a.getArmorType() == ArmorType.MAIL) {
            equipment.put(a.getSlot(), a);
            return true;
        } else {
            throw new InvalidArmorException("This hero can only equip PLATE and MAIL armor");
        }
    }

    @Override
    public void levelUp() {
        attributes.increaseVitality(5);
        attributes.increaseStrength(3);
        attributes.increaseDexterity(2);
        attributes.increaseIntelligence(1);
        level++;
    }
}

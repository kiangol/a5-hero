package items;

import attributes.PrimaryAttribute;
import hero.InvalidArmorException;
import hero.InvalidWeaponException;
import hero.Slot;
import hero.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    @Test
    void equipArmor_HighLevelArmor_ShouldThrowInvalidArmorException() {
        Warrior warrior = new Warrior();
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setLevel(2);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(1,0,0,2));
        assertThrows(InvalidArmorException.class, () -> {
            assert warrior.equipArmor(testPlateBody);
        });
    }

    @Test
    void equipArmor_WrongArmorType_ShouldThrowInvalidArmorException() {
        Warrior warrior = new Warrior();
        Armor testClothBody = new Armor();
        testClothBody.setName("Common Cloth Armor");
        testClothBody.setLevel(1);
        testClothBody.setSlot(Slot.BODY);
        testClothBody.setArmorType(ArmorType.CLOTH);
        testClothBody.setAttributes(new PrimaryAttribute(1,0,0,2));
        assertThrows(InvalidArmorException.class, () -> {
            assert warrior.equipArmor(testClothBody);
        });
    }

    @Test
    void equipArmor_ValidArmorType_ShouldReturnTrue() {
        Warrior warrior = new Warrior();
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(2,1,0,0));
        assertTrue(warrior.equipArmor(testPlateBody));
    }
}
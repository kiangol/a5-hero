package items;

import attributes.PrimaryAttribute;
import hero.Slot;

public abstract class Item {

    private String name;
    private int level;
    private Slot slot;
    private ItemType itemType;

    // Primary Attributes
    PrimaryAttribute attributes;

    public Item(String name) {
        this.name = name;
    }

    public Item() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setAttributes(PrimaryAttribute attributes) {
        this.attributes = attributes;
    }

    public int getStrength() {
        return attributes.getStrength();
    }

    public int getDexterity() {
        return attributes.getDexterity();
    }

    public int getIntelligence() {
        return attributes.getIntelligence();
    }

    public int getVitality() {
        return attributes.getVitality();
    }
}

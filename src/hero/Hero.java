package hero;

import attributes.PrimaryAttribute;
import items.*;

import java.util.HashMap;

public abstract class Hero {
    protected String name;
    protected int level = 1;

    protected HeroType heroType;

    // Store equipments
    protected HashMap<Slot, Item> equipment = new HashMap<>();

    // Primary Attributes
    protected PrimaryAttribute attributes;

    public Hero(String name, PrimaryAttribute attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public Hero(PrimaryAttribute attributes) {
        this.attributes = attributes;
    }

    public Hero() {}

    public double getDPS() {
        double primaryAttribute = getPrimaryAttribute();
        double weaponDPS = 1.0;
        for (Slot slot : equipment.keySet()) {
            Item i = equipment.get(slot);
            if (i instanceof Weapon) {
                weaponDPS = ((Weapon) i).getDPS();
            } else if (i instanceof Armor) {
                primaryAttribute += getArmorAttribute((Armor) i);
            }
        }
        return weaponDPS * (1.0 + (primaryAttribute / 100));
    }

    protected abstract int getArmorAttribute(Armor armor);

    protected abstract int getPrimaryAttribute();

    @Override
    public String toString() {
        return "Hero{" +
            "name='" + getName() + '\'' +
            ", type=" + getHeroType() +
            ", level=" + getLevel() +
            ", strength=" + attributes.getStrength() +
            ", dexterity=" + attributes.getDexterity() +
            ", intelligence=" + attributes.getIntelligence() +
            ", vitality=" + attributes.getVitality() +
            '}';
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroType getHeroType() {
        return heroType;
    }

    public void setHeroType(HeroType heroType) {
        this.heroType = heroType;
    }


    /**
     * Equip a weapon. Level handling is done in the superclass, while other checks are in the respective subclass
     * @param weapon the weapon to equip
     * @throws InvalidWeaponException if the weapon level requirement is higher than the hero's level
     * @return true if passed
     */
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getLevel() > level) {
            throw new InvalidWeaponException("The weapon requires level " + weapon.getLevel() + " but the hero is level " + level);
        }
        return true;
    }

    /**
     * Equip armor. Level handling is done in the superclass, while other checks are in the respective subclass
     * @param armor the piece of armor to equip
     * @throws InvalidArmorException if the armor level requirement is higher than the hero's level
     */
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getLevel() > level) {
            throw new InvalidArmorException("The armor requires level " + armor.getLevel() + " but the hero is level " + level);
        }
        return true;
    }

    public abstract void levelUp();
}
package hero;

import attributes.PrimaryAttribute;
import items.*;

public class Ranger extends Hero {
    public Ranger(String name) {
        super(name, new PrimaryAttribute(8,1,7,1));
        setHeroType(HeroType.RANGER);
    }

    public Ranger() {
        setHeroType(HeroType.RANGER);
    }

    @Override
    protected int getArmorAttribute(Armor armor) {
        return armor.getDexterity();
    }

    @Override
    protected int getPrimaryAttribute() {
        return attributes.getDexterity();
    }

    @Override
    public boolean equipWeapon(Weapon w) throws InvalidWeaponException {
        super.equipWeapon(w);
        if (w.getWeaponType() == WeaponType.BOWS) {
            equipment.put(w.getSlot(), w);
        } else {
            throw new InvalidWeaponException("This hero can only equip BOWS, weapon is " + w.getWeaponType());
        }
        return false;
    }

    @Override
    public boolean equipArmor(Armor a) throws InvalidArmorException {
        super.equipArmor(a);
        if (a.getArmorType() == ArmorType.LEATHER || a.getArmorType() == ArmorType.MAIL) {
            equipment.put(a.getSlot(), a);
            return true;
        } else {
            throw new InvalidArmorException("This hero can only equip LEATHER and MAIL armor");
        }
    }

    @Override
    public void levelUp() {
        System.out.println("RANGER LEVEL UP");
        attributes.increaseVitality(2);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(5);
        attributes.increaseIntelligence(1);
        level++;
    }
}

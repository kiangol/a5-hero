package hero;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
